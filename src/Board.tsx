import { AnimatePresence } from "framer-motion";
import { observer } from "mobx-react";
import React from "react";

import Chooser from "./Chooser";
import Row from "./Row";
import StoreProvider from "./Store";

export default observer((() => (
  <div className={"board"}>
    <div className="rows">
      <p className="amnt">
        <b>{StoreProvider.rows.length}</b> / 10 rows
      </p>
      <AnimatePresence initial={false}>
        {StoreProvider.rows.map((v, i) => (
          <Row
            row={v}
            i={i}
            c={StoreProvider.rows.length}
            key={Math.random()}
          ></Row>
        ))}
      </AnimatePresence>
    </div>

    <Chooser></Chooser>
  </div>
)) as React.FC);
