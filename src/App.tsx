import "./style/App.scss";

import { AnimatePresence, motion } from "framer-motion";
import { observer } from "mobx-react";
import React from "react";

import Board from "./Board";
import Lose from "./Lose";
import StoreProvider from "./Store";
import Win from "./Win";

export default observer((() => (
  <div className={"App"}>
    <div className="header">
      <div className="inner">
        <h1>Mastermind</h1>
        <p>Can you crack the code?</p>
      </div>
    </div>

    <AnimatePresence>
      {(() => {
        if (!StoreProvider.win && !StoreProvider.lose) {
          return (
            <motion.div
              initial={{
                opacity: 0
              }}
              animate={{
                opacity: 1
              }}
              exit={{
                opacity: 0
              }}
              transition={{
                duration: 1,
                ease: "easeInOut"
              }}
              className={"boardParent"}
              key={"board"}
            >
              <Board></Board>
            </motion.div>
          );
        } else if (StoreProvider.win) {
          return (
            <motion.div
              initial={{
                opacity: 0
              }}
              animate={{
                opacity: 1
              }}
              exit={{
                opacity: 0
              }}
              transition={{
                duration: 1,
                ease: "easeInOut"
              }}
              className={"end"}
              key={"end"}
            >
              <Win></Win>
            </motion.div>
          );
        } else if (StoreProvider.lose) {
          return (
            <motion.div
              initial={{
                opacity: 0
              }}
              animate={{
                opacity: 1
              }}
              exit={{
                opacity: 0
              }}
              transition={{
                duration: 1,
                ease: "easeInOut"
              }}
              className={"end"}
              key={"end"}
            >
              <Lose></Lose>
            </motion.div>
          );
        }
      })()}
    </AnimatePresence>
  </div>
)) as React.FC);
