import { action, observable } from "mobx";

function random() {
  return Math.floor(Math.random() * 6);
}

export interface IRow {
  colors: (number | null)[];
  clues: (boolean | null)[];
}

const rowNew: IRow = {
  colors: [null, null, null, null],
  clues: [null, null, null, null]
};

export class Store {
  @observable win = false;
  @observable lose = false;

  @observable code: [number, number, number, number] = [0, 0, 0, 0];

  @observable rows: IRow[] = [];

  @action addColor(color: number) {
    const { colors } = this.rows[0];

    let filled = 0;

    for (let i in colors) {
      if (colors[i] == null) {
        colors[i] = color;
        break;
      }
    }

    for (let i in colors) {
      if (colors[i] !== null) filled++;
    }

    if (filled === 4) this.giveHints();
  }

  @action giveHints() {
    const { colors, clues } = this.rows[0];

    let codeCopy = [...this.code];

    let curClue;

    curClue = 0;
    for (let i in colors) {
      // correct color, incorrect spot
      const index = codeCopy.indexOf(colors[i] as number);
      if (index > -1) {
        codeCopy[index] = -1; // disallow duplicates
        clues[curClue] = false;
        curClue++;
      }
    }

    curClue = 0;
    for (let i in colors) {
      // correct color, correct spot
      if (colors[i] === this.code[i]) {
        codeCopy[i] = -1; // disallow duplicates
        clues[curClue] = true;
        curClue++;
      }
    }

    let correct = 0;
    for (let i in clues) {
      if (clues[i]) correct++;
    }

    if (correct === 4) this.win = true;
    else this.addRow();
  }

  @action addRow() {
    if (this.rows.length < 10) this.rows.unshift(rowNew);
    else this.lose = true;
  }

  @action removeColor(index: number) {
    this.rows[0].colors[index] = null;
  }

  @action reset() {
    this.win = false;
    this.lose = false;
    this.rows = [rowNew];
    this.code = [random(), random(), random(), random()];
    console.log(this.code); // for debugging
  }

  constructor() {
    this.reset();
  }
}

export default new Store();
