import React from "react";

import StoreProvider from "./Store";

export default (() => (
  <div className="inner">
    <h1>You won!</h1>
    <p>Congratulations</p>
    <button onClick={() => StoreProvider.reset()}>Play again?</button>
  </div>
)) as React.FC;
