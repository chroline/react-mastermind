import React from "react";

import StoreProvider from "./Store";

export default (() => (
  <div className="inner">
    <h1>You lost!</h1>
    <p>Better luck next time</p>
    <button onClick={() => StoreProvider.reset()}>Play again?</button>
  </div>
)) as React.FC;
