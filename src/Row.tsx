import classnames from "classnames";
import { motion } from "framer-motion";
import { observer } from "mobx-react";
import React from "react";

import StoreProvider, { IRow } from "./Store";
import colorList from "./style/colorList";

interface RowProps {
  row: IRow;
  i: number;
  c: number;
}

interface SocketProps {
  socket: number | null;
  i: number;
  allow: boolean;
}

interface ClueProps {
  clue: boolean | null;
}

const Socket: React.FC<SocketProps> = props => (
  <button
    aria-label={"socket-" + (props.i + 1)}
    className="socket"
    style={{ backgroundColor: colorList[props.socket as number] }}
    onClick={() => {
      if (props.allow) StoreProvider.removeColor(props.i);
    }}
  ></button>
);

const Clue: React.FC<ClueProps> = props => (
  <div
    className={classnames("clue", { ["_" + props.clue]: props.clue !== null })}
  >
    {(() => {
      switch (props.clue) {
        case true:
          return <img src="/check.svg" alt="" />;
      }
    })()}
  </div>
);

export default observer<React.FC<RowProps>>(((props: RowProps) => (
  <motion.div
    className="row"
    id={props.i + ""}
    initial={{
      y: props.i > 0 ? (props.i - 1) * 100 - 50 : -50,
      opacity: props.i > 0 ? 1 / (props.i ^ (1 / 2)) : 0
    }}
    animate={{ y: props.i * 100 - 50, opacity: 1 / ((props.i + 1) ^ (1 / 2)) }}
    exit={{}}
    transition={{
      duration: 1,
      ease: "easeInOut",
      delay: (props.c - props.i) * 0.1 + 1
    }}
  >
    <div className="inner">
      <div className="sockets">
        {props.row.colors.map((v, j) => {
          return (
            <Socket socket={v} i={j} key={j} allow={props.i === 0}></Socket>
          );
        })}
      </div>
      <div className="clues">
        {props.row.clues.map((v, j) => {
          return <Clue clue={v} key={j}></Clue>;
        })}
      </div>
    </div>
  </motion.div>
)) as React.FC);
