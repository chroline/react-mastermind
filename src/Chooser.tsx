import React from "react";

import StoreProvider from "./Store";
import colorList from "./style/colorList";

const Opt: React.FC<{ i: number }> = props => (
  <div className="option">
    <button
      aria-label={"color-" + (props.i + 1)}
      style={{ backgroundColor: colorList[props.i] }}
      onClick={() => StoreProvider.addColor(props.i)}
    ></button>
  </div>
);

export default () => (
  <div className="chooser">
    <div className="bg"></div>
    <div className="inner">
      <div className="colors">
        {colorList.map((_, i) => {
          return <Opt i={i} key={i}></Opt>;
        })}
      </div>
    </div>
  </div>
);
