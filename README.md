# Mastermind

![Netlify Status](https://api.netlify.com/api/v1/badges/e22be5f8-58e9-4960-b31f-dccacb270320/deploy-status)
![License MIT](https://img.shields.io/badge/license-MIT-blue.svg?style=flat)
![Lighthouse score: 100/100](https://lighthouse-badge.appspot.com/?score=100)
![PRs Welcome](https://img.shields.io/badge/PRs-welcome-brightgreen.svg?style=flat)

React implementation of the Mastermind game.

# Features

- built with [CRA](https://github.com/facebook/create-react-app)
- modern TypeScript
- uses MobX for global state management
- SASS styling
- uses framer-motion for animations
- [perfect Lighthouse house score](https://gitlab.com/chroline/react-mastermind/blob/master/lighthouse-proof.json)
- full PWA support

# Usage

This project is proudly hosted by Netlify. The game is available at [https://mastermind-react.netlify.com](https://mastermind-react.netlify.com)

[![Play](https://img.shields.io/badge/Play-purple.svg?style=for-the-badge)](https://mastermind-react.netlify.com)

# Todo

- document game mechanics
- comment important parts of code
- add E2E and unit tests

# Authors

Created by [Cole Gawin](https://gitlab.com/chroline).
